<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2017-2019 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International.
 */

namespace DarCas\ZfAid\Mvc\Controller\Plugin;

use DarCas\ZfAid\Stdlib;
use Interop\Container\ContainerInterface;
use Zend\Mvc\Controller\Plugin\AbstractPlugin AS ZAbstractPlugin;

/**
 * Class AbstractPlugin
 * @package DarCas\ZfAid\Mvc\Controller\Plugin
 */
abstract class AbstractPlugin extends ZAbstractPlugin
{
    use Stdlib\ServiceManagerTrait;

    /**
     * @param \Interop\Container\ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->setServiceManager($container);
    }
}
