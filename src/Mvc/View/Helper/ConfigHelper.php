<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2017 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International.
 */

namespace DarCas\ZfAid\Mvc\View\Helper;

use DarCas\ZfAid\Stdlib;
use Zend\View\Helper\AbstractHelper;

/**
 * Class ConfigHelper
 * @package DarCas\ZfAid\Mvc\View\Helper
 */
class ConfigHelper extends AbstractHelper
{
    use Stdlib\ConfigTrait;
    use Stdlib\ServiceManagerTrait;

    /**
     * @param \Interop\Container\ContainerInterface $container
     *
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(\Interop\Container\ContainerInterface $container)
    {
        $this->setServiceManager($container);
    }

    /**
     * @param string $path
     * @param mixed $default
     *
     * @return \DarCas\ZfAid\Stdlib\ArrayUtils\DotNotation|mixed
     */
    public function __invoke($path = null, $default = null)
    {
        if (is_null($path)) {
            return $this->getAppConfig();
        } else {
            return $this->getAppConfig($path, $default);
        }
    }
}
