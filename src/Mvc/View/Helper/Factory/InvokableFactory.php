<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2017-2019 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International.
 */

namespace DarCas\ZfAid\Mvc\View\Helper\Factory;

use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class InvokableFactory
 * @package DarCas\ZfAid\Mvc\View\Helper\Factory
 */
class InvokableFactory implements FactoryInterface
{
    /**
     * @param \Interop\Container\ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     *
     * @return object
     */
    public function __invoke(\Interop\Container\ContainerInterface $container, $requestedName, array $options = null)
    {
        return new $requestedName($container, $options);
    }
}
