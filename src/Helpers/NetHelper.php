<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2017-2019 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International.
 */

namespace DarCas\ZfAid\Helpers;

/**
 * Class NetHelper
 * @package DarCas\ZfAid\Helpers
 */
abstract class NetHelper
{
    const CHECK_DOMAIN_LEVEL_TLD = 0;
    const CHECK_DOMAIN_LEVEL_DOMAIN = 1;
    const CHECK_DOMAIN_LEVEL_FIRST = -1;
    const CHECK_DOMAIN_LEVEL_LAST = -2;

    /**
     * HTTP Error Code Translation Table
     *
     * @param int $code
     *
     * @return string
     */
    public static function HTTP_ErrorCode($code)
    {
        /** @var array $http_error_code */
        $http_error_code = [
            # Informational 1xx
            100 => 'Continue',
            101 => 'Switching Protocols',
            102 => 'Processing',
            103 => 'Early Hints',
            # Successful 2xx
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            207 => 'Multi-Status',
            208 => 'Already Reported',
            218 => 'This is fine',
            226 => 'IM Used',
            # Redirection 3xx
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            308 => 'Permanent Redirect',
            # Client Error 4xx
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Payload Too Large',
            414 => 'URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Range Not Satisfiable',
            417 => 'Expectation Failed',
            418 => 'Authentication Required',
            419 => 'Page Expired',
            420 => 'Method Failure',
            421 => 'Misdirected Request',
            422 => 'Unprocessable Entity',
            423 => 'Locked',
            424 => 'Failed Dependency',
            425 => 'Too Early',
            426 => 'Upgrade Required',
            428 => 'Precondition Required',
            429 => 'Too Many Requests',
            431 => 'Request Header Fields Too Large',
            440 => 'Login Time-out',
            449 => 'Retry With',
            451 => 'Unavailable For Legal Reasons',
            498 => 'Invalid Token',
            499 => 'Token Required',
            # Server Error 5xx
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported',
            506 => 'Variant Also Negotiates',
            507 => 'Insufficient Storage',
            508 => 'Loop Detected',
            509 => 'Bandwidth Limit Exceeded',
            510 => 'Not Extended',
            511 => 'Network Authentication Required',
        ];

        return array_key_exists($code, $http_error_code) ? $http_error_code[$code] : 'unknown';
    }

    /**
     * Build a URL by its components
     *
     * @param array $components
     *
     * @return string
     */
    public static function build_url(array $components)
    {
        $return = '';
        if (isSet($components[PHP_URL_SCHEME])) {
            $return .= "{$components[PHP_URL_SCHEME]}://";
        }
        if (isSet($components[PHP_URL_USER])) {
            $return .= "{$components[PHP_URL_USER]}:";
        }
        if (isSet($components[PHP_URL_PASS])) {
            $return .= "{$components[PHP_URL_PASS]}@";
        }
        if (isSet($components[PHP_URL_HOST])) {
            $return .= $components[PHP_URL_HOST];
        }
        if (isSet($components[PHP_URL_PORT])) {
            $return .= ":{$components[PHP_URL_PORT]}";
        }
        if (isSet($components[PHP_URL_PATH])) {
            $return .= "/{$components[PHP_URL_PATH]}";
        }
        if (isSet($components[PHP_URL_QUERY])) {
            if (is_array($components[PHP_URL_QUERY])) {
                $return .= '?' . http_build_query($components[PHP_URL_QUERY]);
            } else {
                $return .= "?{$components[PHP_URL_QUERY]}";
            }
        }

        return $return;
    }

    /**
     * @param string $needle
     * @param int $level
     * @param string|null $serverName
     *
     * @return bool
     */
    public static function check_domain(string $needle, int $level, string $serverName = null)
    {
        if (is_null($serverName)) {
            $serverName = $_SERVER['SERVER_NAME'];
        }

        /** @var array $parts */
        $parts = explode('.', $serverName);
        /** @var int $part */
        $part = $level;

        switch ($level) {
            case static::CHECK_DOMAIN_LEVEL_FIRST:
            case static::CHECK_DOMAIN_LEVEL_TLD:
                $part = count($parts) - 1;
                break;

            case static::CHECK_DOMAIN_LEVEL_DOMAIN:
                $part = count($parts) - 2;
                break;

            case static::CHECK_DOMAIN_LEVEL_LAST:
                $part = 0;
                break;
        }

        return ($parts[$part] == $needle);
    }
}
