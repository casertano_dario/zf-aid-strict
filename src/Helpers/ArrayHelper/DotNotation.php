<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2017-2019 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International.
 */

namespace DarCas\ZfAid\Helpers\ArrayHelper;

/**
 * Class DotNotation
 * @package DarCas\ZfAid\Helpers\ArrayHelper
 */
class DotNotation
{
    /**
     * @var array
     */
    protected $array = [];

    /**
     * @param array $values
     */
    public function __construct(array $values)
    {
        $this->array = $values;
    }

    /**
     * @param string $path
     * @param mixed $default
     *
     * @return mixed
     */
    public function __invoke(string $path, $default = null)
    {
        return $this->get($path, $default);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->array;
    }

    /**
     * @param string $path
     * @param mixed $default
     *
     * @return mixed
     */
    public function get(string $path, $default = null)
    {
        $array = $this->array;

        if (!empty($path)) {
            $keys = explode('.', $path);

            foreach ($keys as $key) {
                if (isset($array[$key])) {
                    $array = $array[$key];
                } else {
                    return $default;
                }
            }
        }

        return $array;
    }

    /**
     * @param string $path
     * @param mixed $value
     *
     * @return $this
     * @throws \RuntimeException
     */
    public function set(string $path, $value)
    {
        $link = &$this->array;

        if (!empty($path)) {
            $keys = explode('.', $path);

            foreach ($keys as $key) {
                if (!isset($link[$key])) {
                    $link[$key] = array();
                }

                $link = &$link[$key];
            }
        }

        $link = $value;

        return $this;
    }

    /**
     * @param string $path
     * @param array $values
     *
     * @return $this
     * @throws \RuntimeException
     */
    public function add(string $path, array $values)
    {
        $get = (array)$this->get($path);

        return $this->set($path, ArrayHelper::mergeRecursiveDistinct($get, $values));
    }

    /**
     * @param mixed $parameters
     *
     * @return $this
     * @throws \RuntimeException
     */
    public function merge($parameters)
    {
        return $this->add(null, (array)$parameters);
    }

    /**
     * @param string $path
     *
     * @return bool
     */
    public function have(string $path)
    {
        $keys = explode('.', $path);
        $array = $this->array;

        foreach ($keys as $key) {
            if (isset($array[$key])) {
                $array = $array[$key];
            } else {
                return false;
            }
        }

        return true;
    }
}
