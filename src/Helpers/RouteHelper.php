<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2017-2020 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International.
 */

namespace DarCas\ZfAid\Helpers;

use Zend\Router\Http;

/**
 * Class RouteHelper
 * @package DarCas\ZfAid\Helpers
 */
abstract class RouteHelper
{
    /**
     * Simple CRUD implementation
     *
     * @param string $single_id_name
     * @param array|null $tools
     * @param array|null $merge
     *
     * @return array
     */
    public static function crud(string $single_id_name = 'id', array $tools = null, array $merge = null)
    {
        /** @var array $route */
        $route = [
            'create' => [
                'type' => Http\Literal::class,
                'options' => [
                    'route' => '/create',
                    'defaults' => [
                        'action' => 'form',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'create' => [
                        'type' => Http\Literal::class,
                        'options' => [
                            'route' => '/create',
                            'defaults' => [
                                'action' => 'create',
                            ],
                        ],
                    ],
                ],
            ],
            'single' => [
                'type' => Http\Segment::class,
                'options' => [
                    'route' => "/:{$single_id_name}",
                    'constraints' => [
                        $single_id_name => '[0-9]+',
                    ],
                    'defaults' => [
                        'action' => 'form',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'update' => [
                        'type' => Http\Literal::class,
                        'options' => [
                            'route' => '/update',
                            'defaults' => [
                                'action' => 'update',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'status' => [
                                'type' => Http\Segment::class,
                                'options' => [
                                    'route' => '/status/:status',
                                    'constraints' => [
                                        'status' => '[a-zA-Z_-]+',
                                    ],
                                    'defaults' => [
                                        'action' => 'set-status',
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'delete' => [
                        'type' => Http\Literal::class,
                        'options' => [
                            'route' => '/delete',
                            'defaults' => [
                                'action' => 'delete',
                            ],
                        ],
                    ],
                ],
            ],
            'trash' => [
                'type' => Http\Literal::class,
                'options' => [
                    'route' => '/trash',
                    'defaults' => [
                        'action' => 'trash-can',
                    ],
                ],
            ],
            'update' => [
                'type' => Http\Literal::class,
                'options' => [
                    'route' => '/update',
                    'defaults' => [
                        'action' => 'update',
                    ],
                ],
                'child_routes' => [
                    'status' => [
                        'type' => Http\Segment::class,
                        'options' => [
                            'route' => '/status/:status',
                            'constraints' => [
                                'status' => '[a-zA-Z_-]+',
                            ],
                            'defaults' => [
                                'action' => 'set-status',
                            ],
                        ],
                    ],
                ],
            ],
            'delete' => [
                'type' => Http\Literal::class,
                'options' => [
                    'route' => '/delete',
                    'defaults' => [
                        'action' => 'delete',
                    ],
                ],
            ],
        ];

        if (!is_null($merge)) {
            $route = ArrayHelper::mergeRecursiveDistinct($route, $merge);
        }

        if (!is_null($tools)) {
            $route['tools'] = [
                'type' => Http\Literal::class,
                'options' => [
                    'route' => '/tools',
                ],
                'child_routes' => $tools,
            ];
        }

        return $route;
    }

    /**
     * CRUD implementation with REST architecture
     *
     * @param string $single_id_name
     * @param array|null $merge
     * @param string $single_id_constraint
     *
     * @return array
     */
    public static function rest(string $single_id_name = 'id', array $merge = null, string $single_id_constraint = '[0-9]+')
    {
        /** @var array $route */
        $route = [
            'create' => [
                'type' => Http\Method::class,
                'options' => [
                    'verb' => 'post',
                    'defaults' => [
                        'action' => 'post',
                    ],
                ],
            ],
            'read' => [
                'type' => Http\Method::class,
                'options' => [
                    'verb' => 'get',
                    'defaults' => [
                        'action' => 'get',
                    ],
                ],
            ],
            'single' => [
                'type' => Http\Segment::class,
                'options' => [
                    'route' => "/:{$single_id_name}",
                    'constraints' => [
                        $single_id_name => $single_id_constraint,
                    ],
                ],
                'child_routes' => [
                    'read' => [
                        'type' => Http\Method::class,
                        'options' => [
                            'verb' => 'get',
                            'defaults' => [
                                'action' => 'get',
                            ],
                        ],
                    ],
                    'update' => [
                        'type' => Http\Method::class,
                        'options' => [
                            'verb' => 'put',
                            'defaults' => [
                                'action' => 'put',
                            ],
                        ],
                    ],
                    'delete' => [
                        'type' => Http\Method::class,
                        'options' => [
                            'verb' => 'delete',
                            'defaults' => [
                                'action' => 'delete',
                            ],
                        ],
                    ],
                ],
            ],
        ];

        if (!is_null($merge)) {
            $route = ArrayHelper::mergeRecursiveDistinct($route, $merge);
        }

        return $route;
    }
}
