<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2019 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International.
 */

namespace DarCas\ZfAid\Stdlib;

/**
 * Trait ViewHelperManagerTrait
 * @package DarCas\ZfAid\Stdlib
 *
 * @method \Zend\ServiceManager\ServiceManager getServiceManager()
 */
trait ViewHelperManagerTrait
{
    /**
     * @return \Zend\View\HelperPluginManager
     */
    protected function getViewHelperManager()
    {
        return $this->getServiceManager()->get('ViewHelperManager');
    }
}
