<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2017-2019 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International.
 */

namespace DarCas\ZfAid\Stdlib;

use DarCas\ZfAid\Helpers\StringHelper;

/**
 * Trait MagicTrait
 * @package DarCas\ZfAid\Stdlib
 */
trait MagicTrait
{
    /**
     * @param string $name
     * @param array $arguments
     *
     * @return mixed
     * @throws \BadMethodCallException
     * @throws \InvalidArgumentException
     */
    public function __call(string $name, array $arguments)
    {
        if (preg_match('#^(set|get)([A-Z])(.*)$#', $name, $matches)) {
            /** @var string $property */
            $property = mb_strtolower($matches[2]) . $matches[3];

            if ($matches[1] == 'get') {
                return $this->__get($property);
            } elseif ($matches[1] == 'set') {
                $this->__set($property, $arguments[0]);

                return $this;
            }
        }

        throw new \BadMethodCallException("Undefined method {$name}");
    }

    /**
     * @param string $name
     *
     * @return mixed
     * @throws \InvalidArgumentException
     */
    public function __get(string $name)
    {
        if (property_exists($this, $name)) {
            if (is_string($this->$name) && StringHelper::isProbablyJson($this->$name)) {
                return json_decode($this->$name);
            }

            if (is_string($this->$name)) {
                if (mb_check_encoding($this->$name, 'UTF-8')) {
                    return $this->$name;
                } else {
                    return mb_convert_encoding($this->$name, 'UTF-8');
                }
            } else {
                return $this->$name;
            }
        } else {
            throw new \InvalidArgumentException("Undefined property {$name}");
        }
    }

    /**
     * @param string $name
     * @param mixed $value
     *
     * @throws \InvalidArgumentException
     */
    public function __set(string $name, $value)
    {
        if (property_exists($this, $name)) {
            $this->$name = $value;
        } else {
            throw new \InvalidArgumentException("Undefined property {$name}");
        }
    }
}
