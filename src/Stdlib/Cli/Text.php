<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2019 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International.
 */

namespace DarCas\ZfAid\Stdlib\Cli;

use Zend\Console\ColorInterface;

/**
 * Trait Text
 * @package DarCas\ZfAid\Stdlib\Cli
 */
trait Text
{
    /**
     * @param array $header
     * @param array $dataTable
     *
     * @return $this
     */
    protected function table(array $header, array $dataTable)
    {
        /** @var array $keys */
        $keys = array_keys($header);
        /** @var array $colSizes */
        $colSizes = array_map(function ($v) {
            return mb_strlen($v);
        }, $header);

        /** @var array $recordSet */
        foreach ($dataTable as $recordSet) {
            /**
             * @var string $key
             * @var string $v
             */
            foreach ($recordSet as $key => $v) {
                /** @var int $len */
                $len = mb_strlen($v);
                if ($colSizes[$key] < $len) {
                    $colSizes[$key] = $len;
                }
            }
        }

        # HEADER
        $this->getConsole()->write('┌');

        foreach ($header as $k => $col) {
            if ($k !== $keys[0]) {
                $this->getConsole()->write('┬');
            }
            $this->getConsole()->write(str_repeat('─', $colSizes[$k] + 2));
        }

        $this->getConsole()->writeLine('┐');

        /**
         * @var int $k
         * @var string $col
         */
        foreach ($header as $k => $col) {
            if ($k === $keys[0]) {
                $this->getConsole()->write('│');
            }
            $this->getConsole()->write(' ');
            $this->getConsole()->write(str_pad($col, $colSizes[$k], ' ', STR_PAD_RIGHT), ColorInterface::MAGENTA);
            $this->getConsole()->write(' ');
            $this->getConsole()->write('│');
        }

        $this->getConsole()->writeLine();

        if (count($dataTable)) {
            /** @var array $recordSet */
            foreach ($dataTable as $recordSet) {
                $this->getConsole()->write('├');

                /**
                 * @var int $k
                 * @var string $col
                 */
                foreach ($header as $k => $col) {
                    if ($k !== $keys[0]) {
                        $this->getConsole()->write('┼');
                    }
                    $this->getConsole()->write(str_repeat('─', $colSizes[$k] + 2));
                }

                $this->getConsole()->writeLine('┤');

                /**
                 * @var int $k
                 * @var mixed $v
                 */
                foreach ($recordSet as $k => $v) {
                    if ($k === $keys[0]) {
                        $this->getConsole()->write('│');
                    }

                    if (is_numeric($v)) {
                        $pad_type = STR_PAD_LEFT;
                    } else {
                        $pad_type = STR_PAD_RIGHT;
                    }

                    $this->getConsole()->write(' ');
                    $this->getConsole()->write(str_pad($v, $colSizes[$k], ' ', $pad_type), ColorInterface::LIGHT_WHITE);
                    $this->getConsole()->write(' ');
                    $this->getConsole()->write('│');
                }

                $this->getConsole()->writeLine('');
            }

            # FOOTER
            $this->getConsole()->write('└');

            /**
             * @var int $k
             * @var string $col
             */
            foreach ($header as $k => $col) {
                if ($k !== $keys[0]) {
                    $this->getConsole()->write('┴');
                }

                $this->getConsole()->write(str_repeat('─', $colSizes[$k] + 2));
            }

            $this->getConsole()->writeLine('┘');
        } else {
            $this->getConsole()->write('├');

            /**
             * @var int $k
             * @var string $col
             */
            foreach ($header as $k => $col) {
                if ($k !== $keys[0]) {
                    $this->getConsole()->write('┴');
                }

                $this->getConsole()->write(str_repeat('─', $colSizes[$k] + 2));
            }

            $this->getConsole()->writeLine('┤');
            $this->getConsole()->write('│');
            $this->getConsole()->write(str_pad('No data available',
                array_sum(array_values($colSizes)) + count($colSizes) + (count($colSizes) * 2) - 1, ' ', STR_PAD_BOTH),
                ColorInterface::LIGHT_RED);
            $this->getConsole()->writeLine('│');

            # FOOTER
            $this->getConsole()->write('└');

            /**
             * @var int $k
             * @var string $col
             */
            foreach ($header as $k => $col) {
                if ($k != $keys[0]) {
                    $this->getConsole()->write('─');
                }

                $this->getConsole()->write(str_repeat('─', $colSizes[$k] + 2));
            }

            $this->getConsole()->writeLine('┘');
        }

        return $this;
    }
}
