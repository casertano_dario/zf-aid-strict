<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2019 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International.
 */

namespace DarCas\ZfAid\Stdlib;

/**
 * Trait ViewRendererTrait
 * @package DarCas\ZfAid\Stdlib
 */
trait ViewRendererTrait
{
    /**
     * @return \Zend\View\Renderer\PhpRenderer
     */
    protected function getViewRenderer()
    {
        return $this->getServiceManager()->get('ViewRenderer');
    }
}
