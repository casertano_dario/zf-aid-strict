<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2017-2019 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International.
 */

namespace DarCas\ZfAid\Stdlib;

use Doctrine\ORM\EntityManager;

/**
 * Trait EntityManagerTrait
 * @package DarCas\ZfAid\Stdlib
 */
trait EntityManagerTrait
{
    /**
     * @var array|null
     */
    protected $cacheEntityManager = null;

    /**
     * @param \Doctrine\ORM\EntityManager $entityManager
     * @param string $orm_name
     *
     * @return $this
     */
    protected function setEntityManager(EntityManager $entityManager, $orm_name = 'orm_default')
    {
        /** @var string $cacheName */
        $cacheName = implode('/', [EntityManagerTrait::class, $orm_name]);

        $this->cacheEntityManager[$cacheName] = $entityManager;

        return $this;
    }

    /**
     * @param string $orm_name
     *
     * @return \Doctrine\ORM\EntityManager
     * @throws \Doctrine\ORM\ORMException
     * @throws \InvalidArgumentException
     */
    protected function getEntityManager(string $orm_name = 'orm_default')
    {
        /** @var string $cacheName */
        $cacheName = implode('/', [EntityManagerTrait::class, $orm_name]);
        /** @var \Doctrine\ORM\EntityManager $entityManager */
        $entityManager = $this->cacheEntityManager[$cacheName];

        if (!$entityManager->isOpen()) {
            /** @var \Doctrine\DBAL\Connection $connection */
            $connection = $entityManager->getConnection();
            /** @var \Doctrine\ORM\Configuration $config */
            $config = $entityManager->getConfiguration();

            $this->cacheEntityManager[$cacheName] = $entityManager::create($connection, $config);
        }

        return $this->cacheEntityManager[$cacheName];
    }
}
