<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2017-2019 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International.
 */

namespace DarCas\ZfAid\Stdlib;

use Interop\Container\ContainerInterface;

/**
 * Trait ServiceManagerTrait
 * @package DarCas\ZfAid\Stdlib
 */
trait ServiceManagerTrait
{
    /**
     * @var array|null
     */
    protected $cacheServiceManager = null;

    /**
     * @param \Interop\Container\ContainerInterface $container
     *
     * @return $this
     */
    protected function setServiceManager(ContainerInterface $container)
    {
        if (!isset($this->cacheServiceManager)) {
            $this->cacheServiceManager = $container;
        }

        return $this;
    }

    /**
     * @return \Zend\ServiceManager\ServiceManager
     */
    protected function getServiceManager()
    {
        return $this->cacheServiceManager;
    }
}
