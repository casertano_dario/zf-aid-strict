<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2017-2019 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International.
 */

namespace DarCas\ZfAid\Stdlib;

use DarCas\ZfAid\Helpers\ArrayHelper;

/**
 * Trait ConfigTrait
 * @package DarCas\ZfAid\Stdlib
 */
trait ConfigTrait
{
    /**
     * @var array|null
     */
    protected $cacheConfig = null;

    /**
     * @param string|null $path
     * @param mixed $default
     *
     * @return \DarCas\ZfAid\Helpers\ArrayHelper\DotNotation|mixed
     */
    public function getAppConfig(string $path = null, $default = null)
    {
        if (!isset($this->cacheConfig[ConfigTrait::class])) {
            /** @var array $config */
            $config = $this->getServiceManager()->get('Config');

            $this->cacheConfig[ConfigTrait::class] = new ArrayHelper\DotNotation($config);
        }

        /** @var \DarCas\ZfAid\Helpers\ArrayHelper\DotNotation $config */
        $config = $this->cacheConfig[ConfigTrait::class];

        if (is_null($path)) {
            return $config;
        } else {
            return $config->get($path, $default);
        }
    }
}
