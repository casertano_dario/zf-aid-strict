<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2017 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International.
 */

namespace DarCas\ZfAid\Console\Controller;

/**
 * Interface AbstractActionController
 * @package DarCas\ZfAid\Console\Controller
 */
interface AbstractActionControllerInterface
{
    /**
     * @param string $target
     *
     * @return array
     */
    public static function getRouteConfig($target);
}
