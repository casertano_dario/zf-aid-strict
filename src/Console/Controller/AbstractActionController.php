<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2017-2019 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International.
 */

namespace DarCas\ZfAid\Console\Controller;

use DarCas\ZfAid\Stdlib;
use Interop\Container\ContainerInterface;
use Zend\Console\Adapter\AdapterInterface;
use Zend\Console\ColorInterface;
use ZF\Console\Route;

/**
 * Class AbstractActionController
 * @package DarCas\ZfAid\Console\Controller
 */
abstract class AbstractActionController implements AbstractActionControllerInterface
{
    use Stdlib\ServiceManagerTrait;
    /**
     * @var \Zend\Console\Adapter\AdapterInterface
     */
    protected $console;
    /**
     * @var \ZF\Console\Route
     */
    protected $route;

    /**
     * @param \Interop\Container\ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->setServiceManager($container);
    }

    /**
     * @param \ZF\Console\Route $route
     * @param \Zend\Console\Adapter\AdapterInterface $console
     *
     * @return mixed
     */
    public function __invoke(Route $route, AdapterInterface $console)
    {
        $this->console = $console;
        $this->route = $route;

        /** @var string $action */
        $action = $route->getMatchedParam('action', 'not-found');
        /** @var string $method */
        $method = static::getMethodFromAction($action);

        if (!method_exists($this, $method)) {
            $method = 'notFoundAction';
        }

        return $this->$method();
    }

    /**
     * @return void
     */
    public function notFoundAction()
    {
        /** @var \Zend\Console\Adapter\AdapterInterface $console */
        $console = $this->getConsole();
        $console->writeLine('Action not found', ColorInterface::RED);
        $console->writeLine();
    }

    /**
     * Transform an "action" token into a method name
     *
     * @param  string $action
     *
     * @return string
     */
    public static function getMethodFromAction($action)
    {
        $method = str_replace(['.', '-', '_'], ' ', $action);
        $method = ucwords($method);
        $method = str_replace(' ', '', $method);
        $method = lcfirst($method);
        $method .= 'Action';

        return $method;
    }

    /**
     * @return \Zend\Console\Adapter\AdapterInterface
     */
    protected function getConsole()
    {
        return $this->console;
    }

    /**
     * @return \ZF\Console\Route
     */
    protected function getRoute()
    {
        return $this->route;
    }

    /**
     * @param string $param
     * @param mixed $default
     *
     * @return mixed
     */
    protected function getParam($param, $default = null)
    {
        return $this->getRoute()->getMatchedParam($param, $default);
    }
}
