<?php
/**
 * @author      Dario Casertano <dario@casertano.name>
 * @copyright   Copyright (c) 2017-2019 Casertano Dario – All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International.
 */

namespace DarCas\ZfAid;

use DarCas\ZfAid\Stdlib;

/**
 * Class AbstractEntity
 * @package DarCas\ZfAid\Entity
 *
 * @property \DateTime|null $insertDate
 * @property \DateTime|null $updateDate
 */
abstract class AbstractEntity
{
    use Stdlib\EntityManagerTrait {
        setEntityManager as public;
        getEntityManager as public;
    }
    use Stdlib\MagicTrait;
    use Stdlib\ServiceManagerTrait {
        setServiceManager as public;
        getServiceManager as public;
    }

    /**
     * @param string|null $format null for \DateTime object
     *
     * @return \DateTime|null|string
     */
    public function getInsertDate(string $format = null)
    {
        if (is_null($this->insertDate)) {
            return null;
        } else {
            if (is_null($format)) {
                return $this->insertDate;
            } else {
                return $this->insertDate->format($format);
            }
        }
    }

    /**
     * @param string|null $format null for \DateTime object
     *
     * @return \DateTime|null|string
     */
    public function getUpdateDate(string $format = null)
    {
        if (is_null($this->updateDate)) {
            return null;
        } else {
            if (is_null($format)) {
                return $this->updateDate;
            } else {
                return $this->updateDate->format($format);
            }
        }
    }
}
